class AdminController < ApplicationController
  
  def store_list
  @stores = Store.all
  end
  
  def store_new
  @store = Store.new  
  end
  
  def store_create
    @store = Store.new(params[:store])
    
    if @store.save
        #se guardo con exito
        redirect_to :action => 'store_list'      
      else
        #algun error y regresa al formulario
        render :action => 'store_new'
    end
  end

    
  def store_edit
     @store = Store.find(params[:id])
  end
  
  def store_update
    @store = Store.find(params[:id])
    
    if @store.update_attributes(params[:store])
        #redirecciona al listado despues de guardar con exito
        redirect_to :action => 'store_list'
      else  
        #regresa al editar con los datos
        render :action => 'store_edit'
    end
  end
  
  def store_destroy
    Store.find(params[:id]).destroy
    redirect_to :action => 'store_list'       
  end
  
  ###################################Articulos

  def article_list
       #va enlistar todas las tiendas

       #hace un select * from articles y guarda la coleccion de objetos en @tiendas
       @articles = Article.all
  end

  def article_new
  #va a crear una tienda nueva
    #crea un objeto de tipo article para poder asignarle los valores en la vista
    @article = Article.new
  end


  def article_create
    #recibir la objeto con los datos capturados en article_new
    @article = Article.new(params[:article])

    if @article.save
        redirect_to :action => 'article_list'      
      else
        render :action => 'article_new'
    end      
  end



  def article_edit
  @article = Article.find(params[:id])
  end



  def article_update
    @article = Article.find(params[:id])

    if @article.update_attributes(params[:article])
        #redirecciona al listado despues de guardar con exito
        redirect_to :action => 'article_list'
      else  
        #regresa al editar con los datos
        render :action => 'article_edit'
    end

  end


  def article_destroy
    Article.find(params[:id]).destroy
    redirect_to :action => 'article_list'       
  end
  
  def make_list
       #va enlistar todas las tiendas

       #hace un select * from makes y guarda la coleccion de objetos en @tiendas
       @makes = Make.all
  end
 ###################################Marcas
  def make_new
  #va a crear una tienda nueva
    #crea un objeto de tipo make para poder asignarle los valores en la vista
    @make = Make.new
  end


  def make_create
    #recibir la objeto con los datos capturados en make_new
    @make = Make.new(params[:make])

    if @make.save
        redirect_to :action => 'make_list'      
      else
        render :action => 'make_new'
    end      
  end



  def make_edit
  @make = Make.find(params[:id])
  end



  def make_update
    @make = Make.find(params[:id])

    if @make.update_attributes(params[:make])
        #redirecciona al listado despues de guardar con exito
        redirect_to :action => 'make_list'
      else  
        #regresa al editar con los datos
        render :action => 'make_edit'
    end

  end


  def make_destroy
    Make.find(params[:id]).destroy
    redirect_to :action => 'make_list'       
  end

  def make_view
  @make = Make.find(params[:id])
  end
  
  def model_new 
  @model = Model.new
  @makes = Make.all
  @make = Make.find(params[:make])
  end
  
  def model_create
  @model = Model.new(params[:model])
  @make = Make.find(params[:id])
  @make.models << @model
  redirect_to :action => 'make_view', :id => @make.id 
  end
  
 def model_edit
  @model = Model.find(params[:id])
  end  
  
  def model_update
  @model = Model.find(params[:id])

  if @model.update_attributes(params[:model])
    redirect_to :action => 'make_view', :id => @model.make.id
  else
    render_to :action => 'model_edit'
  end  
  end
  
  def model_destroy
    @model = Model.find(params[:id])
    Model.find(params[:id]).destroy
    redirect_to :action => 'make_view', :id => @model.make.id      
  end
  
  
end
