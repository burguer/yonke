class Article < ActiveRecord::Base
  attr_accessible :name
  has_many :store_articles
end
