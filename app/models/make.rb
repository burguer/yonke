class Make < ActiveRecord::Base
  attr_accessible :name
  has_many :store_articles
  has_many :models
end
